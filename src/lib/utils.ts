import { type ClassValue, clsx } from "clsx";
import { NextPageContext } from "next";
import { twMerge } from "tailwind-merge";

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}

export const PAGE_SIZE = 25;
const separator = "__";

export const months = [
  "enero",
  "febrero",
  "marzo",
  "abril",
  "mayo",
  "junio",
  "julio",
  "agosto",
  "septiembre",
  "octubre",
  "noviembre",
  "diciembre",
];

export const createBusinessSlug = ({
  name,
  id,
}: {
  name: string;
  id: string;
}) => `${encodeURIComponent(name)}${separator}${id}`;

export const parseBusinessSlug = (slug?: string) => {
  if (!slug) return { name: "", id: "" };
  const [name, id] = slug.split(separator);
  const decodedName = decodeURIComponent(name);
  return { name: decodedName, id };
};

export const generateCSV = (data: object[]) => {
  let csvContent = "";
  const refinedData = [];

  refinedData.push(Object.keys(data[0]));
  data.forEach((item) => {
    refinedData.push(Object.values(item));
  });

  refinedData.forEach((row) => {
    csvContent += row.join(",") + "\n";
  });

  return csvContent;
};

export const isClient = (ctx: NextPageContext) => !ctx.req;
