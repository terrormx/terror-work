import { createQueryKeyStore } from "@lukemorales/query-key-factory";

import { PAGE_SIZE } from "./utils";

import { Business, Category, Label, Report, State } from "@/types";
import api from "@/utils/axios/client";

type Context = {
  pageParam: string | null;
};

// API Reference
// https://postgrest.org/en/v12/references/api/tables_views.html#read

export const queries = createQueryKeyStore({
  categories: {
    all: {
      queryKey: ["categories"],
      queryFn: async (): Promise<Category[]> => {
        const response = await api.get<Category[]>("categories", {
          params: {
            order: "active.desc",
          },
        });
        return response.data;
      },
    },
  },
  states: {
    all: {
      queryKey: ["states"],
      queryFn: async (): Promise<State[]> => {
        const response = await api.get<State[]>("states");
        return response.data;
      },
    },
  },
  businesses: {
    search: (query: string, column: string) => ({
      queryKey: [column, query],
      queryFn: async (): Promise<State[]> => {
        const response = await api.get<State[]>("businesses", {
          params: {
            [column]: `plfts.${query}`,
          },
        });
        return response.data;
      },
    }),
    list: ({
      categoryId,
      stateId,
    }: {
      categoryId?: string;
      stateId?: string;
    }) => ({
      queryKey: [categoryId],
      queryFn: async ({ pageParam }: Context): Promise<Business[]> => {
        const response = await api.get<Business[]>("normalized_businesses", {
          params: {
            category_id: `eq.${categoryId}`,
            state_id: `eq.${stateId}`,
            limit: `${PAGE_SIZE}`,
            offset: `${pageParam}`,
            order: "normalized_name.asc",
          },
        });
        return response.data;
      },
      contextQueries: {
        count: () => ({
          queryKey: ["count"],
          queryFn: async (): Promise<number> => {
            const response = await api.get<Report[]>("businesses", {
              headers: {
                Prefer: "count=exact",
              },
              params: {
                category_id: `eq.${categoryId}`,
                state_id: `eq.${stateId}`,
                limit: "1",
              },
            });
            const [, count] = response.headers["content-range"].split("/");
            return count as number;
          },
        }),
      },
    }),

    detail: (id: string) => ({
      queryKey: [id],
      queryFn: async (): Promise<Business> => {
        const response = await api.get<Business[]>("businesses", {
          params: {
            id: `eq.${id}`,
          },
        });
        return response.data[0];
      },
      contextQueries: {
        labels: {
          queryKey: ["labels"],
          queryFn: async (): Promise<string[]> => {
            const response = await api.get<Pick<Report, "labels">[]>(
              "reports",
              {
                params: {
                  business_id: `eq.${id}`,
                  select: "labels",
                },
              },
            );
            return Array.from(
              new Set(response.data.map(({ labels }) => labels).flat()),
            );
          },
        },
        reports: (label?: string) => ({
          queryKey: [label],
          queryFn: async ({ pageParam }: Context): Promise<Report[]> => {
            const params: Record<string, string> = {
              business_id: `eq.${id}`,
              limit: `${PAGE_SIZE}`,
              offset: `${pageParam}`,
              order: "created_at.desc",
            };

            if (!!label) {
              params.labels = `cs.{${label}}`;
            }
            const response = await api.get<Report[]>("reports", {
              params,
            });
            return response.data;
          },
          contextQueries: {
            count: () => ({
              queryKey: ["count"],
              queryFn: async (): Promise<number> => {
                const params: Record<string, string> = {
                  business_id: `eq.${id}`,
                  limit: "1",
                };

                if (!!label?.length) {
                  params.labels = `cs.{${label}}`;
                }

                const response = await api.get<Report[]>("reports", {
                  headers: {
                    Prefer: "count=exact",
                  },
                  params,
                });
                const [, count] = response.headers["content-range"].split("/");
                return count as number;
              },
            }),
          },
        }),
      },
    }),
  },
  reports: {
    detail: (id: string) => ({
      queryKey: [id],
      queryFn: async (): Promise<Report> => {
        const response = await api.get<Report[]>("reports", {
          params: {
            id: `eq.${id}`,
          },
        });
        return response.data[0];
      },
    }),
  },
  labels: {
    all: {
      queryKey: ["all"],
      queryFn: async (): Promise<Label[]> => {
        const response = await api.get<Label[]>("labels");
        return response.data;
      },
    },
  },
});
