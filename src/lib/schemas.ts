import { z } from "zod";

const defaultError = {
  required_error: "Requerido",
};
const ID = z.string(defaultError).uuid();

export const BusinessSchema = z.object({
  id: z.string().uuid(),
  name: z
    .string(defaultError)
    .transform((val) => val.toLocaleUpperCase().trim()),
  stateId: z.string(defaultError),
  categoryId: z.string(defaultError),
  createdAt: z.date(defaultError),
  updatedAt: z.date(defaultError),
  verifiedAt: z.date(defaultError),
  ig: z
    .string(defaultError)
    .regex(
      /^(?:@?)([a-z0-9_](?:(?:[a-z0-9_]|(?:\.(?!\.))){0,28}(?:[a-z0-9_]))?)$/,
      "Usuario inválido",
    )
    .transform((val) => val.toLocaleLowerCase().replace("@", "").trim()),
  url: z
    .string(defaultError)
    .url("URL inválida")
    .optional()
    .transform((val) => val && new URL(val).hostname),
  rfc: z
    .string(defaultError)
    .regex(
      /^([A-ZÑ]|&){3,4}[0-9]{2}(0[1-9]|1[0-2])([12][0-9]|0[1-9]|3[01])[A-Z0-9]{3}$/,
      "RFC inválido",
    )
    .optional()
    .transform((val) => val?.toUpperCase().trim()),
  reportCount: z.number(defaultError),
});

export const ReportSchema = z.object({
  id: ID,
  businessId: ID,
  title: z.string(defaultError).max(120, "Máximo 120 caracteres"),
  body: z.string(defaultError).min(120, "Mínimo 120 caracteres"),
  onBehalf: z.boolean(defaultError),
  hasProof: z.boolean(defaultError),
  labels: z.array(z.string(defaultError)),
  seniority: z
    .number(defaultError)
    .positive("Mayor a 0")
    .lte(50, "Máximo 50 años")
    .optional(),
  createdAt: z.date(defaultError),
  updatedAt: z.date(defaultError),
});

export const StateSchema = z.object({
  id: z.string(defaultError),
  name: z.string(defaultError),
  createdAt: z.date(defaultError),
  active: z.boolean(defaultError),
});

export const LabelSchema = z.object({
  id: z.string(defaultError),
  name: z.string(defaultError),
  parentId: z.string(defaultError).optional(),
  createdAt: z.date(defaultError),
});

export const CategorySchema = z.object({
  id: z.string(defaultError),
  name: z.string(defaultError),
  color: z.string(defaultError),
  createdAt: z.date(defaultError),
  active: z.boolean(defaultError),
});

export const BusinessFormSchema = BusinessSchema.pick({
  name: true,
  stateId: true,
  categoryId: true,
  url: true,
  ig: true,
  rfc: true,
});

export const ReportFormSchema = ReportSchema.pick({
  businessId: true,
  title: true,
  body: true,
  onBehalf: true,
  hasProof: true,
  labels: true,
  seniority: true,
});
