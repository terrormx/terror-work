import { NextRequest, NextResponse } from "next/server";

import { Ratelimit } from "@upstash/ratelimit";
import { kv } from "@vercel/kv";

const ipRatelimit = new Ratelimit({
  redis: kv,
  limiter: Ratelimit.slidingWindow(1, "1 d"),
});
const fingerPrintRatelimit = new Ratelimit({
  redis: kv,
  limiter: Ratelimit.slidingWindow(1, "1 d"),
});

export const config = {
  matcher: ["/api/businesses", "/api/reports"],
};

export default async function middleware(request: NextRequest) {
  const fingerprint = request.headers.get("X-Device-Fingerprint") ?? "unknown";
  const ip = request.ip ?? "127.0.0.1";

  if (!fingerprint) {
    return NextResponse.json({ code: "forbidden" }, { status: 403 });
  }

  const ipLimit = await ipRatelimit.limit(ip + request.url);
  const fingerPrintLimit = await fingerPrintRatelimit.limit(
    fingerprint + request.url,
  );

  return ipLimit.success && fingerPrintLimit.success
    ? NextResponse.next()
    : NextResponse.json({ code: "too_many_requests" }, { status: 429 });
}
