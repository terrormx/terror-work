import { z } from "zod";

import {
  BusinessSchema,
  CategorySchema,
  LabelSchema,
  ReportSchema,
  StateSchema,
} from "@/lib/schemas";

export type Label = z.infer<typeof LabelSchema>;
export type Business = z.infer<typeof BusinessSchema>;
export type Report = z.infer<typeof ReportSchema>;
export type State = z.infer<typeof StateSchema>;
export type Category = z.infer<typeof CategorySchema>;
