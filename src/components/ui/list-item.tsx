import { cn } from "@/lib/utils";

export default function ListItem({
  children,
  className,
  ...otherProps
}: React.HTMLAttributes<HTMLDivElement>) {
  return (
    <div
      {...otherProps}
      className={cn(
        "h-16 flex items-center px-4 text-xl uppercase truncate",
        className,
      )}
    >
      {children}
    </div>
  );
}
