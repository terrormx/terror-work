"use client";

import * as CollapsiblePrimitive from "@radix-ui/react-collapsible";

import { cn } from "@/lib/utils";

const Collapsible = CollapsiblePrimitive.Root;

const CollapsibleTrigger = ({
  children,
  open,
  ...props
}: CollapsiblePrimitive.CollapsibleTriggerProps & { open: boolean }) => (
  <CollapsiblePrimitive.CollapsibleTrigger
    {...props}
    className={cn("flex w-full justify-between items-center", props.className)}
  >
    {children}
    <span>{open ? "●" : "◌"}</span>
  </CollapsiblePrimitive.CollapsibleTrigger>
);

const CollapsibleContent = CollapsiblePrimitive.CollapsibleContent;

export { Collapsible, CollapsibleTrigger, CollapsibleContent };
