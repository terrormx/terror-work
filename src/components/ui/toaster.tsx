"use client";

import Paper from "./paper";

import * as Icons from "@/components/ui/icon";
import { BlackCat, Love } from "@/components/ui/icon";
import {
  Toast,
  ToastDescription,
  ToastProvider,
  ToastTitle,
  ToastViewport,
} from "@/components/ui/toast";
import { useToast } from "@/components/ui/use-toast";
import { cn } from "@/lib/utils";

export function Toaster() {
  const { toasts, dismiss } = useToast();

  return (
    <ToastProvider duration={3000}>
      {toasts.map(function ({
        id,
        title,
        description,
        action,
        icon = "Barbed",
        ...props
      }) {
        const rotateClassName = `${Math.random() > 0.5 ? "-" : ""}rotate-${[2, 3, 6, 12][Math.floor(Math.random() * 4)]}`;
        const Icon = Icons[icon];

        return (
          <Toast
            key={id}
            {...props}
            onClick={() => dismiss()}
            className={rotateClassName}
          >
            <Paper
              className={cn(
                "flex gap-4 items-center -rotate-2 px-10 py-6 w-full",
                props.variant === "dark" && "bg-dark text-light",
              )}
            >
              <Icon className="w-1/4 max-h-20 shrink-0" />
              <div className="shrink flex flex-col gap-2">
                {title && <ToastTitle>{title}</ToastTitle>}
                {description && (
                  <ToastDescription>{description}</ToastDescription>
                )}
              </div>
            </Paper>
            {action}
          </Toast>
        );
      })}
      <ToastViewport onClick={() => dismiss()} />
    </ToastProvider>
  );
}
