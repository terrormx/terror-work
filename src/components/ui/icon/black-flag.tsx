import React from "react";

interface Props extends React.SVGProps<SVGSVGElement> {
  className?: string;
  cutOut?: boolean;
}

const BlackFlag: React.FC<Props> = ({ cutOut, ...otherProps }) => {
  if (cutOut)
    return (
      <svg
        viewBox="0 0 487 497"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...otherProps}
      >
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M26 0L337.5 25.5L486.5 275L337.5 313.5L260.5 497L171 426.5L0.5 132.5L26 0ZM127.751 103.577L292.671 114.849L352.248 236.143L207.14 247.37L233.59 353.423L127.751 103.577Z"
          fill="currentColor"
        />
      </svg>
    );
  return (
    <svg
      viewBox="0 0 226 251"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...otherProps}
    >
      <path
        d="M165.672 11.8489L0.750977 0.576904L106.59 250.423L80.14 144.37L225.248 133.143L165.671 11.8489H165.672Z"
        fill="currentColor"
      />
    </svg>
  );
};

export default BlackFlag;
