import React from "react";

interface Props extends React.SVGProps<SVGSVGElement> {
  className?: string;
  cutOut?: boolean;
}

const FlagFactory: React.FC<Props> = ({ cutOut, ...otherProps }) => {
  if (cutOut)
    return (
      <svg
        viewBox="0 0 433 425"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...otherProps}
      >
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M29 0.5L229.5 40.5L401.5 0.5L433 213L401.5 425L29 402L0 213L29 0.5ZM292.596 117.824L331.569 100.1L335.939 194.41L166.187 202.281L172.615 126.374L161.812 107.974L292.596 117.824ZM326.382 202.811L326.384 202.809H326.382L326.382 202.811ZM326.382 202.811L267.901 250.293L266.914 207.558L220.203 251.507L218.315 210.806L165.557 253.045L155.626 103.286L124.806 103.719L118.06 322.9L327.8 319.143L326.382 202.811Z"
          fill="currentColor"
        />
      </svg>
    );

  return (
    <svg
      viewBox="0 0 218 223"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...otherProps}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M208.384 102.809L149.901 150.293L148.914 107.558L102.203 151.507L100.315 110.806L47.5571 153.045L37.6261 3.286L6.80606 3.71899L0.0600586 222.9L209.8 219.143L208.382 102.809H208.384Z"
        fill="currentColor"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M213.569 0.100006L174.596 17.824L43.812 7.974L54.615 26.374L48.187 102.281L217.939 94.41L213.569 0.100006Z"
        fill="currentColor"
      />
    </svg>
  );
};

export default FlagFactory;
