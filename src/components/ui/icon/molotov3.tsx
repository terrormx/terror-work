import React from "react";

interface Props extends React.SVGProps<SVGSVGElement> {
  className?: string;
  cutOut?: boolean;
}

const Molotov3: React.FC<Props> = ({ cutOut, ...otherProps }) => {
  if (cutOut)
    return (
      <svg
        viewBox="0 0 379 396"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...otherProps}
      >
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M74 85L0.5 314L192.5 396L229 333.5L379 283L353.5 119L319.5 0H192.5L74 85ZM265.529 63.203L295.497 119.731L308.137 200.117L281.542 220.565L256.377 207.568L255.048 118.712L233.096 155.693L224.87 200.07L148.58 301.795L89.863 259.888L156.361 153.534L194.428 129.442L220.578 95.295L233.049 79.7L265.529 63.203ZM179.561 189.578L156.333 172.572L111.092 239.516L134.057 255.968L179.561 189.578ZM263.956 199.37L282.817 210.97L299.554 196.958L285.058 121.778L264.168 74.728L241.943 86.74L253.82 95.425L265.704 93.9L263.956 199.37Z"
          fill="currentColor"
        />
      </svg>
    );
  return (
    <svg
      viewBox="0 0 220 239"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...otherProps}
    >
      <path
        d="M206.498 56.732L176.529 0.203003L144.049 16.7L131.578 32.295L105.428 66.442L67.361 90.534L0.863037 196.888L59.58 238.795L135.87 137.07L144.096 92.693L166.048 55.712L167.377 144.568L192.542 157.565L219.137 137.117L206.497 56.731L206.498 56.732ZM67.333 109.572L90.561 126.578L45.057 192.968L22.092 176.516L67.333 109.572ZM193.817 147.97L174.956 136.37L176.704 30.9L164.82 32.425L152.943 23.74L175.168 11.728L196.058 58.778L210.554 133.958L193.817 147.97Z"
        fill="currentColor"
      />
    </svg>
  );
};

export default Molotov3;
