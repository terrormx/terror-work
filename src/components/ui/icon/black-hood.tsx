import React from "react";

interface Props extends React.SVGProps<SVGSVGElement> {
  cutOut?: boolean;
}

const BlackHood: React.FC<Props> = ({ cutOut, ...otherProps }) => {
  if (cutOut)
    return (
      <svg
        viewBox="0 0 419 486"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...otherProps}
      >
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M252 0.5L419 155.5L370.5 486L0 468V73.5L252 0.5ZM196.963 113.659L228.77 119.181V119.182L288.369 178.579L293.911 354.805L101.887 358.981L100.089 208.888L131.552 138.918L196.963 113.659ZM128.32 217.899L151.909 255.617L189.627 232.031L128.32 217.899ZM185.915 334.392H208.662L229.755 307.722L208.012 291.064H185.268L164.174 317.734L185.915 334.392ZM204.3 232.031L242.018 255.617L265.607 217.899L204.3 232.031Z"
          fill="currentColor"
        />
      </svg>
    );

  return (
    <svg
      viewBox="0 0 194 246"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...otherProps}
    >
      <path
        d="M128.77 6.18094L96.963 0.658936L31.552 25.9179L0.0889893 95.8879L1.88698 245.981L193.911 241.805L188.369 65.5789L128.77 6.18195V6.18094ZM51.909 142.617L28.32 104.899L89.627 119.031L51.909 142.617ZM108.662 221.392H85.915L64.174 204.734L85.268 178.064H108.012L129.755 194.722L108.662 221.392ZM142.018 142.617L104.3 119.031L165.607 104.899L142.018 142.617Z"
        fill="currentColor"
      />
    </svg>
  );
};

export default BlackHood;
