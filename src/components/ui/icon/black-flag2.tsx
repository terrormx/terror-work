import React from "react";

interface Props extends React.SVGProps<SVGSVGElement> {
  className?: string;
  cutOut?: boolean;
}

const BlackFlag2: React.FC<Props> = ({ cutOut, ...otherProps }) => {
  if (cutOut)
    return (
      <svg
        viewBox="0 0 518 522"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...otherProps}
      >
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M0 343L129.5 0.5L518 182L357 409.5H273.5L129.5 521.5L0 343ZM165.454 243.131L106.229 369.084L90.959 376.661L201.193 136.339L284.821 176.821L373.041 212.234L322.249 321.722L165.454 243.131Z"
          fill="currentColor"
        />
      </svg>
    );

  return (
    <svg
      viewBox="0 0 166 249"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...otherProps}
    >
      <path
        d="M39.537 117.757L55.213 130.383L67.84 114.702L52.154 102.088L39.537 117.757Z"
        fill="currentColor"
      />
      <path
        d="M165.973 41.4199L119.099 21.839L74.386 0.194946L25.984 105.716L24.825 103.405L16.221 107.213L20.766 117.094L12.618 134.859L8.46898 143.904L0.0269775 162.308L3.18698 163.753L11.273 146.126L30.659 161.491L47.394 155.403L54.429 183.174L34.484 206.453L30.041 247.257L49.049 210.914L65.602 193.262L65.669 193.267L71.614 218.601L97.233 248.807L84.535 210.704L78.243 179.536L78.216 179.543L69.607 145.029L36.969 127.624L27.983 109.701L52.601 56.0359L139.173 96.725L165.975 41.421L165.973 41.4199ZM30.392 138.021L44.924 143.438L31.705 148.599L15.491 136.927L22.68 121.256L30.392 138.022V138.021Z"
        fill="currentColor"
      />
    </svg>
  );
};

export default BlackFlag2;
