import React from "react";

interface Props extends React.SVGProps<SVGSVGElement> {
  className?: string;
  cutOut?: boolean;
}

const MolotovCocktail: React.FC<Props> = ({ cutOut, ...otherProps }) => {
  if (cutOut)
    return (
      <svg
        viewBox="0 0 354 425"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...otherProps}
      >
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M0 140L62 297L14.5 425L323 406L278 240L354 126L180.5 0L0 140ZM149.545 137.546L141.82 156.006H122.924L128.046 116.023L132.366 132.233L144.637 129.906L167.523 87.518L190.836 122.072L202.785 106.831L218.199 142.981L211.694 156.006H192.973L184.115 133.281L178.62 140.3L167.844 124.36L157.227 143.945L151.571 145.031L149.545 137.546ZM95.671 159.859L246.33 160.16L177.459 264.637L177.023 323.007L212.954 349.482L128.878 349.424L164.823 323.006L164.577 264.91L95.671 159.859ZM217.593 185.468L229.144 168.881H112.692L124.213 185.468H217.593Z"
          fill="currentColor"
        />
      </svg>
    );
  return (
    <svg
      viewBox="0 0 152 263"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...otherProps}
    >
      <path
        d="M151.33 73.16L0.671021 72.859L69.577 177.91L69.823 236.006L33.878 262.424L117.954 262.482L82.023 236.007L82.459 177.637L151.33 73.16ZM134.144 81.881L122.593 98.468H29.213L17.692 81.881H134.144Z"
        fill="currentColor"
      />
      <path
        d="M46.82 69.006L54.545 50.546L56.571 58.031L62.227 56.945L72.8439 37.36L83.6199 53.3L89.1149 46.281L97.9729 69.006H116.694L123.199 55.981L107.785 19.831L95.836 35.072L72.5229 0.518005L49.6369 42.906L37.366 45.233L33.046 29.023L27.924 69.006H46.82Z"
        fill="currentColor"
      />
    </svg>
  );
};

export default MolotovCocktail;
