import React from "react";

interface Props extends React.SVGProps<SVGSVGElement> {
  className?: string;
  cutOut?: boolean;
}

const Coffin: React.FC<Props> = ({ cutOut, ...otherProps }) => {
  if (cutOut)
    return (
      <svg
        viewBox="0 0 420 480"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...otherProps}
      >
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M78 35L293 0.5L419.5 198.5L322 479.5L78 459.5L0.5 155L78 35ZM157.359 112.046H246.246H246.247L291.678 168.835L249.407 358.955H154.594L112.323 168.835L157.359 112.046ZM211.876 191.057H231.629V171.304H211.876V151.551H192.123V171.304H172.37V191.057H192.123V260.191H211.876V191.057Z"
          fill="currentColor"
        />
      </svg>
    );
  return (
    <svg
      viewBox="0 0 180 247"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...otherProps}
    >
      <path
        d="M134.246 0.04599H45.359L0.322998 56.835L42.594 246.955H137.407L179.678 56.835L134.247 0.04599H134.246ZM119.629 79.057H99.876V148.191H80.123V79.057H60.37V59.304H80.123V39.551H99.876V59.304H119.629V79.057Z"
        fill="currentColor"
      />
    </svg>
  );
};

export default Coffin;
