import React from "react";

interface Props extends React.SVGProps<SVGSVGElement> {
  className?: string;
  cutOut?: boolean;
}

const Bomb: React.FC<Props> = ({ cutOut, ...otherProps }) => {
  if (cutOut)
    return (
      <svg
        viewBox="0 0 435 474"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        {...otherProps}
      >
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M112.5 91L0.5 215.5L49 431L219 473.5L434.5 343L377 91L322 0H191.5L112.5 91ZM215.551 103.673L243.327 91.182L271.194 103.673L285.216 158.667L306.609 160.195V171.697L277.036 166.755L263.014 111.85L243.328 102.684L223.732 111.85L214.563 165.946H231.822V192.275L275.102 221.821L292.228 272.429L267.795 331.394L208.809 355.818L149.823 331.395L125.39 272.429L142.517 221.823L185.797 192.275V165.946H203.056L215.551 103.673ZM172.672 236.218L164.491 228.041L164.492 228.04H164.49L145.524 272.342H157.03L172.672 236.218Z"
          fill="currentColor"
        />
      </svg>
    );

  return (
    <svg
      viewBox="0 0 182 265"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...otherProps}
    >
      <path
        d="M118.327 0.182007L90.551 12.673L78.056 74.946H60.797V101.275L17.517 130.823L0.390015 181.429L24.823 240.395L83.809 264.818L142.795 240.394L167.228 181.429L150.102 130.821L106.822 101.275V74.946H89.563L98.732 20.85L118.328 11.684L138.014 20.85L152.036 75.755L181.609 80.697V69.195L160.216 67.667L146.194 12.673L118.326 0.182007H118.327ZM39.491 137.041L47.672 145.218L32.03 181.342H20.524L39.49 137.04H39.492L39.491 137.041Z"
        fill="currentColor"
      />
    </svg>
  );
};

export default Bomb;
