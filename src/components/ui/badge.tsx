import * as React from "react";
import { cva, type VariantProps } from "class-variance-authority";

import { cn } from "@/lib/utils";

const badgeVariants = cva(
  "cursor-pointer inline-flex flex-none items-center rounded-full border border-dark px-3 py-1 text-sm font-regular transition-colors",
  {
    variants: {
      variant: {
        default: "border-transparent bg-dark text-category",
        secondary: "border-transparent bg-neutral-100 text-dark",
        destructive: "border-transparent bg-red-500 text-category",
        outline: "text-dark",
      },
    },
    defaultVariants: {
      variant: "default",
    },
  },
);

export interface BadgeProps
  extends React.HTMLAttributes<HTMLDivElement>,
    VariantProps<typeof badgeVariants> {}

function Badge({ className, variant, ...props }: BadgeProps) {
  return (
    <div className={cn(badgeVariants({ variant }), className)} {...props} />
  );
}

export { Badge, badgeVariants };
