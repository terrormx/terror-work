import NextLink, { LinkProps as NextLinkProps } from "next/link";

import { cn } from "@/lib/utils";

interface LinkProps extends NextLinkProps {
  className?: string;
  children?: React.ReactNode;
  variant?: "default" | "download" | "external";
  target?: string;
}

const variants = {
  default: "↗",
  download: "↓",
  external: "↗",
};

function Link({
  className,
  variant = "default",
  children,
  ...otherProps
}: LinkProps) {
  return (
    <NextLink {...otherProps}>
      <span className={cn("underline", className)}>{children}</span>
      <span>{` ${variants[variant]}`}</span>
    </NextLink>
  );
}

export default Link;
