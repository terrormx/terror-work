import { cn } from "@/lib/utils";

interface PaperProps {
  className?: string;
  children: React.ReactNode;
}

function Paper({ className, children }: PaperProps) {
  return (
    <div className={cn("polygon-1 lines-20 bg-light", className)}>
      {children}
    </div>
  );
}

export default Paper;
