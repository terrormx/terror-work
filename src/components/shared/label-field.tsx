import { Badge } from "@/components/ui/badge";
import useLabels from "@/hooks/useLabels";

interface LabelFieldProps {
  value: string[];
  onChange: (value: string[]) => void;
}

function LabelField({ value, onChange }: LabelFieldProps) {
  const { data } = useLabels();

  const handleParentChange = (parentId: string) => {
    const isTag = value.includes(parentId);
    const childrenIds =
      data?.childrenLabels[parentId].map(({ id: childrenId }) => childrenId) ??
      [];

    const filteredLabels = value.filter((value) => {
      if (value === parentId) return false;
      if (childrenIds.includes(value)) return false;
      return true;
    });

    const nextValue = isTag ? filteredLabels : [...value, parentId];
    onChange(nextValue);
  };

  const handleChildrenChange = (id: string) => {
    const isTag = value.includes(id);
    const nextValue = isTag
      ? value.filter((value) => value !== id)
      : [...value, id];
    onChange(nextValue);
  };

  return (
    <div className="space-y-1">
      <div className="flex flex-wrap gap-1 text-category">
        {data?.parentLabels?.map(({ id, name }) => (
          <Badge
            key={id}
            variant={value.includes(id) ? "default" : "outline"}
            onClick={() => handleParentChange(id)}
          >
            {name}
          </Badge>
        ))}

        {data?.parentLabels?.map(
          ({ id: parentId }) =>
            value.includes(parentId) &&
            data?.childrenLabels?.[parentId]?.map(({ id, name }) => (
              <Badge
                key={id}
                variant={value.includes(id) ? "default" : "outline"}
                onClick={() => handleChildrenChange(id)}
              >
                {name}
              </Badge>
            )),
        )}
      </div>
    </div>
  );
}

export default LabelField;
