import { Fragment } from "react";
import { useRouter } from "next/router";
import { usePostHog } from "posthog-js/react";

import { useQuery } from "@tanstack/react-query";

import * as Icons from "@/components/ui/icon";
import ListItem from "@/components/ui/list-item";
import { Separator } from "@/components/ui/separator";
import { useToast } from "@/components/ui/use-toast";
import { queries } from "@/lib/queries";
import { cn } from "@/lib/utils";
import { Category } from "@/types";

interface Props {
  stateId?: string;
}

const Categories: React.FC<Props> = ({ stateId }: Props) => {
  const router = useRouter();
  const posthog = usePostHog();
  const { toast } = useToast();
  const { data: categories, isLoading } = useQuery(queries.categories.all);

  const handleClick = (category: Category) => {
    posthog.capture("Category Clicked", {
      stateId,
      categoryId: category.id,
      active: category.active,
    });

    if (category.active) {
      router.push(`/${stateId}/${category.id}`);
    } else {
      toast({
        title: "Próximamente",
        icon: ["ThrowMolotov", "ProtestCop", "DestroyColonialism", "Barricade"][
          Math.floor(Math.random() * 3)
        ] as keyof typeof Icons,
        variant: "dark",
        duration: 700,
      });
    }
  };

  return categories?.map((category) => (
    <Fragment key={category.id}>
      <ListItem
        onClick={() => handleClick(category)}
        className={cn("bg-category", category.active && "cursor-pointer")}
      >
        <p className={!category.active ? "opacity-50" : ""}>{category.name}</p>
      </ListItem>
      <Separator />
    </Fragment>
  ));
};

export default Categories;
