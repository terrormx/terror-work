import Head from "@/components/shared/head";
import Credits from "@/components/shared/mdx/credits.mdx";

interface PageLayoutProps {
  children: React.ReactNode;
  title: string;
}

export default function PageLayout({ children, title }: PageLayoutProps) {
  return (
    <div className="p-4 pb-20">
      <Head title={title} />
      {children}
      <Credits />
    </div>
  );
}
