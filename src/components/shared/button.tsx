import { memo } from "react";

import { cn } from "@/lib/utils";

interface ButtonProps extends React.HTMLProps<HTMLButtonElement> {
  children: React.ReactNode;
  type?: "submit" | "button";
  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

function Button({ children, className, ...otherProps }: ButtonProps) {
  return (
    <button
      className={cn(
        "polygon-1 font-display h-16 grow flex items-center justify-center bg-dark text-category uppercase text-xl font-medium active:scale-95",
        className,
      )}
      {...otherProps}
    >
      {children}
    </button>
  );
}

export default memo(Button);
