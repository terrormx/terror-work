import React from "react";
import Head from "next/head";
import { useRouter } from "next/router";
import tinycolor from "tinycolor2";

import { useQuery } from "@tanstack/react-query";

import { queries } from "@/lib/queries";

const ThemeProvider = () => {
  const { query } = useRouter();
  const { categoryId } = query as Record<string, string>;
  const { data: categories } = useQuery(queries.categories.all);
  const category = categories?.find((c) => c.id === categoryId);
  const color = category?.color ?? "#EBE3D4";
  const darkerColor = tinycolor(color).darken(3).toString();
  const lighterColor = tinycolor(color).lighten(20).toString();

  return (
    <>
      <style jsx global>
        {`
          body {
            background-color: ${darkerColor};
            --color-category: ${color};
            --color-category-darker: ${darkerColor};
          }

          ::selection {
            background-color: ${lighterColor};
          }
        `}
      </style>
      <Head>
        <meta name="theme-color" content={darkerColor} />
      </Head>
    </>
  );
};

export default ThemeProvider;
