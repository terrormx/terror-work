import InfiniteScroll from "react-infinite-scroll-component";
import Link from "next/link";

import { useQueryClient } from "@tanstack/react-query";

import Report from "./report";

import EmptyState from "@/components/shared/empty-state";
import LoadingState from "@/components/shared/loading-state";
import ListItem from "@/components/ui/list-item";
import { Separator } from "@/components/ui/separator";
import { queries } from "@/lib/queries";
import { Report as ReportType } from "@/types";

interface ReportsProps {
  data: ReportType[];
  isLoading: boolean;
  hasNextPage: boolean;
  fetchNextPage: () => void;
  stateId: string;
  categoryId: string;
  slug: string;
}

export default function Reports({
  data,
  isLoading,
  hasNextPage,
  stateId,
  categoryId,
  slug,
  fetchNextPage,
}: ReportsProps) {
  const queryClient = useQueryClient();

  if (!data?.length && isLoading) return <LoadingState />;
  if (!data?.length) return <EmptyState message="No hay denuncias todavía." />;

  return (
    <InfiniteScroll
      dataLength={data.length}
      next={fetchNextPage}
      hasMore={hasNextPage}
      loader={<ListItem>Cargando...</ListItem>}
    >
      {data.map((report) => (
        <Link
          key={report.id}
          href={`/${stateId}/${categoryId}/${slug}/reports/${report.id}`}
          onClick={() => {
            queryClient.setQueryData(
              queries.reports.detail(report.id).queryKey,
              () => report,
            );
          }}
        >
          <Report
            title={report.title}
            body={report.body}
            createdAt={report.createdAt}
            variant="compact"
          />
          <Separator />
        </Link>
      ))}
    </InfiniteScroll>
  );
}
