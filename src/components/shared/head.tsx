import NextHead from "next/head";
import { useRouter } from "next/router";

function Head({
  title: customTitle,
  ogImage,
}: {
  title?: string;
  ogImage?: string;
}) {
  const { asPath } = useRouter();
  const title = customTitle ?? "Terror MX";
  const description =
    "Facilitamos la denuncia anónima y segura de abuso laboral.";
  const url = `https://terror.work${asPath}`;

  return (
    <NextHead>
      <title>{title}</title>
      <meta name="description" content={description} />
      <meta name="robots" content="follow, index" />
      <meta name="googlebot" content="index,follow" />

      {/* Open Graph / Facebook */}
      <meta property="og:site_name" content="Terror MX" />
      <meta property="og:title" content={title} />
      <meta property="og:description" content={description} />
      <meta property="og:url" content={url}></meta>
      {ogImage && <meta property="og:image" content={ogImage} />}

      {/* Twitter */}
      {ogImage && <meta property="twitter:image" content={ogImage} />}
      <meta property="twitter:card" content="summary_large_image"></meta>
      <meta property="twitter:title" content={title}></meta>
      <meta property="twitter:description" content={description}></meta>
    </NextHead>
  );
}

export default Head;
