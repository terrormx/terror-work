import { useState } from "react";

import { useQuery } from "@tanstack/react-query";

import { Separator } from "../ui/separator";

import { Alert, AlertDescription } from "@/components/ui/alert";
import {
  Collapsible,
  CollapsibleContent,
  CollapsibleTrigger,
} from "@/components/ui/collapsible";
import useLabelNames from "@/hooks/useLabelNames";
import { queries } from "@/lib/queries";

interface BusinessLinksProps {
  id: string;
  count?: number;
}

function BusinessAlert({ count, id }: BusinessLinksProps) {
  const [open, setOpen] = useState(false);
  const { data: labels } = useQuery(queries.businesses.detail(id)._ctx.labels);
  const labelNames = useLabelNames();
  if (!labels?.length) return null;

  return (
    <Collapsible onOpenChange={setOpen}>
      <Alert className="p-0">
        <AlertDescription>
          <CollapsibleTrigger open={open} className="h-10 px-4">
            Tiene {count} denuncia por...
          </CollapsibleTrigger>
          <CollapsibleContent>
            <Separator className="bg-category" />
            <ul className="text-sm p-4">
              {labels?.map((label) => (
                <li key={label}>* {labelNames[label]}</li>
              ))}
            </ul>
          </CollapsibleContent>
        </AlertDescription>
      </Alert>
    </Collapsible>
  );
}

export default BusinessAlert;
