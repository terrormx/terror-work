interface BottomContainerProps {
  children: React.ReactNode;
}

function BottomContainer({ children }: BottomContainerProps) {
  return <div className="fixed bottom-2 left-2 right-2 flex">{children}</div>;
}

export default BottomContainer;
