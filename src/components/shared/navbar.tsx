import { memo, useEffect, useState } from "react";
import Marquee from "react-fast-marquee";
import Link from "next/link";
import { usePathname } from "next/navigation";

interface TerrorMarqueeProps {
  title: string;
}

const TerrorMarquee = memo(({ title }: TerrorMarqueeProps) => {
  return (
    <div className="bg-category-darker h-16 z-10 border-b border-b-dark">
      <Marquee autoFill className="h-full flex items-center">
        <h1 className="text-5xl uppercase text-dark font-display">
          {title} &nbsp;
        </h1>
      </Marquee>
    </div>
  );
});

TerrorMarquee.displayName = "TerrorMarquee";

export default function Navbar() {
  const pathname = usePathname();
  const [title, setTitle] = useState("");

  useEffect(() => {
    setTitle(document.querySelector("title")?.innerText ?? "Terror MX");
  }, [pathname]);

  return (
    <Link className="sticky top-0 z-10" href="/cmx">
      <TerrorMarquee title={title} />
    </Link>
  );
}
