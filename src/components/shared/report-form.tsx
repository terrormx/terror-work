import { useForm } from "react-hook-form";
import axios, { AxiosError } from "axios";
import { useRouter } from "next/router";
import { usePostHog } from "posthog-js/react";
import { z } from "zod";

import { zodResolver } from "@hookform/resolvers/zod";
import { useMutation, useQueryClient } from "@tanstack/react-query";

import BottomContainer from "./bottom-container";
import LabelField from "./label-field";

import Button from "@/components/shared/button";
import { Checkbox } from "@/components/ui/checkbox";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormLabel,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { Separator } from "@/components/ui/separator";
import { Textarea } from "@/components/ui/textarea";
import { useToast } from "@/components/ui/use-toast";
import { queries } from "@/lib/queries";
import { ReportFormSchema } from "@/lib/schemas";
import { parseBusinessSlug } from "@/lib/utils";
import { Report } from "@/types";
import getFingerprint from "@/utils/fingerprint";

type FormData = z.infer<typeof ReportFormSchema>;

export default function ReportForm() {
  const router = useRouter();
  const { toast, dismiss } = useToast();
  const { slug, categoryId, stateId } = router.query as Record<string, string>;
  const { id: businessId } = parseBusinessSlug(slug);
  const posthog = usePostHog();
  const queryClient = useQueryClient();

  const form = useForm<FormData>({
    resolver: zodResolver(ReportFormSchema),
    defaultValues: {
      businessId,
      onBehalf: false,
      hasProof: false,
      labels: [],
    },
  });

  const { mutate, isPending } = useMutation({
    mutationFn: async (data: FormData): Promise<Report[]> => {
      const fingerprint = await getFingerprint();
      const response = await axios.post(
        "/api/reports",
        {
          ...data,
          businessId,
          // The label indicates the opposite, so we need to negate it
          onBehalf: data.onBehalf ? false : true,
        },
        {
          headers: {
            "X-Device-Fingerprint": fingerprint,
          },
        },
      );
      return response.data;
    },
    onSuccess: (data) => {
      const [newReport] = data;
      queryClient.setQueryData(
        queries.reports.detail(newReport.id).queryKey,
        () => newReport,
      );
      queryClient.invalidateQueries({
        queryKey: queries.businesses.detail(businessId)._ctx.reports._def,
      });
      posthog.capture("Report Created", {
        reportId: newReport.id,
        businessId,
        categoryId,
        stateId,
      });
      router.replace(
        `/${stateId}/${categoryId}/${slug}/reports/${newReport.id}`,
      );
      toast({
        title: "¡Gracias!",
        description:
          "Tus palabras suman a responsabilizar a aquellos que tienen el poder de cambiar la realidad.",
        variant: "default",
        duration: 10000,
        icon: "Love",
      });
    },
    onError: (error: AxiosError) => {
      const statusCode = error?.response?.status;
      posthog.capture("Report Creation Failed", {
        businessId,
        categoryId,
        stateId,
        statusCode,
      });

      if (statusCode === 429) {
        router.replace(`/${stateId}/${categoryId}/${slug}`);
        toast({
          title: "Sólo puedes crear un reporte",
          icon: "BlackCat",
        });
        return;
      }

      toast({
        title: "Algo salió mal. Intenta de nuevo",
        icon: "BlackCat",
      });
    },
  });

  const onSubmit = (data: FormData) => {
    mutate(data);
  };

  return (
    <Form {...form}>
      <form
        onClick={() => dismiss()}
        onSubmit={form.handleSubmit(onSubmit)}
        className="pb-28"
      >
        <FormField
          control={form.control}
          name="title"
          rules={{
            required: true,
          }}
          render={({ field }) => (
            <FormItem>
              <FormControl>
                <Input
                  id="title"
                  type="text"
                  autoFocus
                  placeholder="Introduce un título corto"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <Separator />
        <FormField
          control={form.control}
          name="body"
          rules={{
            required: true,
          }}
          render={({ field }) => (
            <FormItem>
              <FormControl>
                <Textarea
                  id="body"
                  placeholder="Cuenta tu anécdota"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <Separator />
        <FormField
          control={form.control}
          name="seniority"
          rules={{
            required: true,
          }}
          render={({ field }) => (
            <FormItem>
              <FormControl>
                <Input
                  id="seniority"
                  type="number"
                  placeholder="¿Cuántos años has trabajado ahí?"
                  {...field}
                  onChange={(e) => {
                    const value = e.target.value;
                    field.onChange(value ? parseInt(value) : 0);
                  }}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <Separator />
        <FormField
          control={form.control}
          name="labels"
          render={({ field }) => (
            <FormItem className="pt-2 pb-4 px-4">
              <FormControl>
                <div className="space-y-2">
                  <FormLabel>
                    Selecciona las etiquetas que representen las condiciones
                    laborales
                  </FormLabel>
                  <LabelField {...field} />
                </div>
              </FormControl>
            </FormItem>
          )}
        />
        <Separator />
        <div className="p-4 space-y-3">
          <Label>Marca las casillas que consideres</Label>

          <FormField
            control={form.control}
            name="hasProof"
            rules={{
              required: true,
            }}
            render={({ field }) => (
              <FormItem>
                <FormControl>
                  <div className="flex items-center gap-1.5">
                    <Checkbox
                      id="hasProof"
                      checked={field.value}
                      onCheckedChange={field.onChange}
                    />
                    <Label htmlFor="hasProof">Tengo evidencia</Label>
                  </div>
                </FormControl>
              </FormItem>
            )}
          />
          <FormField
            control={form.control}
            name="onBehalf"
            rules={{
              required: true,
            }}
            render={({ field }) => (
              <FormItem>
                <FormControl>
                  <div className="flex items-center gap-1.5">
                    <Checkbox
                      id="onBehalf"
                      checked={field.value}
                      onCheckedChange={field.onChange}
                    />
                    <Label htmlFor="onBehalf">
                      Escribo en nombre de alguien más
                    </Label>
                  </div>
                </FormControl>
              </FormItem>
            )}
          />
        </div>
        <BottomContainer>
          <Button type="submit" disabled={isPending}>
            {isPending ? "Guardando..." : "Publicar"}
          </Button>
        </BottomContainer>
      </form>
    </Form>
  );
}
