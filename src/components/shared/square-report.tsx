import { useQuery } from "@tanstack/react-query";

import { Badge } from "@/components/ui/badge";
import { queries } from "@/lib/queries";

interface SquareReportProps {
  businessName: string;
  id: string;
}

const SquareReport: React.FC<SquareReportProps> = ({ businessName, id }) => {
  const { data } = useQuery(queries.reports.detail(id));

  return (
    <div
      id="shareable"
      className=" text-dark space-y-4 p-4 flex flex-col bg-category"
      style={{ height: "100vw" }}
    >
      <div className="flex-none">
        <h2 className="uppercase text-semibold text-3xl">{businessName}</h2>
      </div>

      <p className="text-ellipsis overflow-hidden">{data?.body}</p>

      <div className="flex-none">
        <Badge>Denuncia en terror.work</Badge>
      </div>
    </div>
  );
};

export default SquareReport;
