import { cn } from "@/lib/utils";

interface IGReportProps {
  id: string;
  body: string;
  businessName: string;
  className?: string;
}

export default function IGReport({
  id,
  body,
  businessName,
  className,
}: IGReportProps) {
  return (
    <div
      id={id}
      className={cn(
        "bg-category w-[1080px] h-[1080px] polka-ig relative",
        className,
      )}
    >
      <div className="bg-category-darker border-b-[1.5px] border-dark px-10 py-2 polka-ig">
        <p className="line-clamp-[1] text-[96px] text-dark font-display uppercase font-medium mb-0">
          Terror en {businessName}
        </p>
      </div>
      <div className="px-10 py-6">
        <p className="line-clamp-[12] font-sans text-dark text-[48px] leading-normal text-balance">
          {body}
        </p>
      </div>
      <div className="absolute right-8 bottom-8 flex shrink-0 pb-8 justify-end -rotate-12">
        <p className="text-5xl polygon-1 text-dark bg-light text-category px-6 py-3 font-display lines">
          terror.work
        </p>
      </div>
    </div>
  );
}
