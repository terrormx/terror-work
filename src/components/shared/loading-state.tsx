import React, { memo } from "react";

const LoadingState: React.FC = () => {
  return (
    <div className="p-4 flex flex-col grow items-center justify-center">
      <p className="text-6xl animate-spin duration-1000">☺</p>
    </div>
  );
};

export default memo(LoadingState);
