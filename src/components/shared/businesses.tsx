import InfiniteScroll from "react-infinite-scroll-component";
import Link from "next/link";

import {
  useInfiniteQuery,
  useQuery,
  useQueryClient,
} from "@tanstack/react-query";

import EmptyState from "@/components/shared/empty-state";
import LoadingState from "@/components/shared/loading-state";
import { Badge } from "@/components/ui/badge";
import ListItem from "@/components/ui/list-item";
import { Separator } from "@/components/ui/separator";
import { queries } from "@/lib/queries";
import { createBusinessSlug, PAGE_SIZE } from "@/lib/utils";
import { Business } from "@/types";

interface BusinessesProps {
  categoryId?: string;
  stateId?: string;
}

export default function Businesses({ categoryId, stateId }: BusinessesProps) {
  const queryClient = useQueryClient();
  const { data: count } = useQuery({
    ...queries.businesses.list({ categoryId, stateId })._ctx.count(),
    enabled: !!categoryId && !!stateId,
  });
  const { data, fetchNextPage, hasNextPage, isLoading } = useInfiniteQuery<
    Business[]
  >({
    ...queries.businesses.list({ categoryId, stateId }),
    initialPageParam: 0,
    getNextPageParam: (lastPage, allPages) => {
      const loadedItems = (allPages?.length ?? 0) * PAGE_SIZE;
      if (loadedItems < (count ?? 0)) return loadedItems;
    },
    enabled: !!categoryId && !!stateId,
  });

  if (!data?.pages[0].length && isLoading) return <LoadingState />;
  if (!data?.pages[0].length)
    return <EmptyState message="No hay negocios registrados." />;

  const items = data.pages.flat();

  return (
    <InfiniteScroll
      dataLength={items.length}
      next={fetchNextPage}
      hasMore={hasNextPage}
      loader={<ListItem>Cargando...</ListItem>}
    >
      {items.map((business) => (
        <Link
          key={business.name}
          href={`/${stateId}/${categoryId}/${createBusinessSlug(business)}`}
          onClick={() => {
            queryClient.setQueryData(
              queries.businesses.detail(business.id).queryKey,
              () => business,
            );
          }}
        >
          <ListItem className="flex justify-between items-center gap-2 w-full bg-category">
            <div className="truncate">{business.name}</div>
            <Badge className="text-xs px-2 rotate-1 polygon-rounded-1">
              {business.reportCount}
            </Badge>
          </ListItem>
          <Separator />
        </Link>
      ))}
    </InfiniteScroll>
  );
}
