import React from "react";

interface TitleProps {
  children?: React.ReactNode;
}

const Title: React.FC<TitleProps> = ({ children }) => {
  return (
    <div tw="flex h-full shrink-0 items-center" style={{ flexBasis: "67%" }}>
      <p
        tw="uppercase m-0 text-[140px] leading-[120px]"
        style={{
          display: "block",
          lineClamp: 4,
          wordBreak: "break-word",
          textWrap: "wrap",
        }}
      >
        Terror {children}
      </p>
    </div>
  );
};

export default Title;
