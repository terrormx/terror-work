import React from "react";

interface RuledPageProps {
  color?: string;
  children?: React.ReactNode;
}

const RuledPage: React.FC<RuledPageProps> = ({ color, children }) => {
  return (
    <div tw="w-full h-full bg-[#171717] text-[#171717] flex items-center p-2">
      <div
        tw="flex items-center text-[#171717] w-full h-full rounded-3xl px-10 py-16"
        style={{
          background: color ?? "#EBE3D4",
        }}
      >
        {children}
        <div tw="absolute border-b border-b-[#17171755] top-[50px] w-[1200px]" />
        <div tw="absolute border-b border-b-[#17171755] top-[100px] w-[1200px]" />
        <div tw="absolute border-b border-b-[#17171755] top-[150px] w-[1200px]" />
        <div tw="absolute border-b border-b-[#17171755] top-[200px] w-[1200px]" />
        <div tw="absolute border-b border-b-[#17171755] top-[250px] w-[1200px]" />
        <div tw="absolute border-b border-b-[#17171755] top-[300px] w-[1200px]" />
        <div tw="absolute border-b border-b-[#17171755] top-[350px] w-[1200px]" />
        <div tw="absolute border-b border-b-[#17171755] top-[400px] w-[1200px]" />
        <div tw="absolute border-b border-b-[#17171755] top-[450px] w-[1200px]" />
        <div tw="absolute border-b border-b-[#17171755] top-[500px] w-[1200px]" />
        <div tw="absolute border-b border-b-[#17171755] top-[550px] w-[1200px]" />
        <div tw="absolute border-b border-b-[#17171755] top-[600px] w-[1200px]" />
        <div tw="absolute border-b border-b-[#17171755] top-[650px] w-[1200px]" />
        <div tw="absolute border-b border-b-[#17171755] top-[700px] w-[1200px]" />
        <div tw="absolute border-b border-b-[#17171755] top-[750px] w-[1200px]" />
        <div tw="absolute border-b border-b-[#17171755] top-[800px] w-[1200px]" />
        <div tw="absolute border-b border-b-[#17171755] top-[850px] w-[1200px]" />
        <div tw="absolute border-b border-b-[#17171755] top-[900px] w-[1200px]" />
        <div tw="absolute border-b border-b-[#17171755] top-[950px] w-[1200px]" />
        <div tw="absolute border-b border-b-[#17171755] top-[1000px] w-[1200px]" />
        <div tw="absolute border-b border-b-[#17171755] top-[1050px] w-[1200px]" />
        <div tw="absolute border-b border-b-[#17171755] top-[1100px] w-[1200px]" />
        <div tw="absolute border-b border-b-[#17171755] top-[1150px] w-[1200px]" />
        <div tw="absolute border-b border-b-[#17171755] top-[1200px] w-[1200px]" />
      </div>
    </div>
  );
};

export default RuledPage;
