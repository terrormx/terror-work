import React, { memo } from "react";

import Link from "@/components/ui/link";

interface EmptyStateProps {
  message: string;
}

const numerals = ["①", "②", "③", "④", "⑤", "⑥", "⑦", "⑧", "⑨", "⓪"];

const EmptyState: React.FC<EmptyStateProps> = ({ message }) => {
  return (
    <div className="p-4 space-y-8">
      <p>{message}</p>
      <div className="text-sm">
        Mientras puedes
        <p>
          ① Leer nuestro <Link href="/core/manifesto">manifesto</Link>
        </p>
        <p>
          ② Conocer nuestros <Link href="/core/principios">principios</Link>
        </p>
      </div>
    </div>
  );
};

export default memo(EmptyState);
