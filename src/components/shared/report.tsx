import { Badge } from "@/components/ui/badge";
import { cn, months } from "@/lib/utils";
import { Label, Report as ReportType } from "@/types";

type ReportProps = Partial<
  Pick<ReportType, "body" | "title" | "createdAt" | "hasProof">
> & {
  variant?: "default" | "compact";
  labels?: Label[];
};

export default function Report({
  title,
  body,
  createdAt,
  hasProof,
  variant = "default",
  labels,
}: ReportProps) {
  const date = createdAt ? new Date(createdAt) : null;

  if (variant === "compact") {
    return (
      <div className="text-dark py-4 space-y-1 px-4 ">
        <p className="uppercase truncate text-base">{title}</p>
        <p className="line-clamp-[10] text-sm">{body}</p>
      </div>
    );
  }
  return (
    <div className="text-dark py-4 space-y-6 px-4">
      <div>
        <p className="uppercase text-2xl mb-2">{title}</p>
        <p>{body}</p>
      </div>

      {labels && (
        <div className="flex gap-1 flex-wrap">
          {hasProof && (
            <Badge className="bg-dark text-category">Existe evidencia</Badge>
          )}
          {labels.map((label) => (
            <Badge
              key={label.id}
              variant="outline"
              className="border-dark text-dark"
            >
              {label.name}
            </Badge>
          ))}
        </div>
      )}

      {date && variant === "default" && (
        <p className="text-xs opacity-50 oldstyle-nums">
          {months[date.getMonth()]} {date.getFullYear()}
        </p>
      )}
    </div>
  );
}
