import { Fragment } from "react";
import { useRouter } from "next/router";
import { usePostHog } from "posthog-js/react";

import { useQuery } from "@tanstack/react-query";

import * as Icons from "@/components/ui/icon";
import ListItem from "@/components/ui/list-item";
import { Separator } from "@/components/ui/separator";
import { useToast } from "@/components/ui/use-toast";
import { queries } from "@/lib/queries";
import { cn } from "@/lib/utils";
import { State } from "@/types";

const States = () => {
  const router = useRouter();
  const posthog = usePostHog();
  const { toast } = useToast();
  const { data: states } = useQuery(queries.states.all);

  const handleClick = (state: State) => {
    posthog.capture("category_click", {
      stateId: state.id,
      active: state.active,
    });

    if (state.active) {
      router.push(`/${state.id}`);
    } else {
      toast({
        title: "Próximamente",
        icon: ["ThrowMolotov", "ProtestCop", "DestroyColonialism", "Barricade"][
          Math.floor(Math.random() * 3)
        ] as keyof typeof Icons,
        variant: "dark",
        duration: 700,
      });
    }
  };

  return states?.map((state) => (
    <Fragment key={state.id}>
      <ListItem
        onClick={() => handleClick(state)}
        className={cn("bg-category", state.active && "cursor-pointer")}
      >
        <p className={!state.active ? "opacity-50" : ""}>{state.name}</p>
      </ListItem>
      <Separator />
    </Fragment>
  ));
};

export default States;
