import { memo } from "react";
import { usePostHog } from "posthog-js/react";

import { Bomb, Megaphone, Molotov3 } from "@/components/ui/icon";
import useShare from "@/hooks/useShare";
import { cn } from "@/lib/utils";

interface ShareButtonProps extends React.HTMLProps<HTMLButtonElement> {
  type?: "submit" | "button";
  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

function ShareButton({ className, ...otherProps }: ShareButtonProps) {
  // const posthog = usePostHog();
  const { isAvailable, shareURL, isSharing } = useShare();
  // const iconName = posthog.getFeatureFlag("share-icon");
  if (!isAvailable) return;

  return (
    <button
      disabled={isSharing}
      className={cn(
        "polygon-rounded-1 shrink-0 text-category font-display h-16 w-16 flex items-center justify-center bg-dark active:scale-95",
        className,
      )}
      {...otherProps}
      onClick={shareURL}
    >
      <Molotov3 className="w-8" />
      {/* {iconName === "bomb" && <Bomb className="w-6 rotate-12" />}
      {iconName === "molotov" && <Molotov3 className="w-8" />}
      {!iconName || (iconName === "control" && <Megaphone className="w-8" />)} */}
    </button>
  );
}

export default memo(ShareButton);
