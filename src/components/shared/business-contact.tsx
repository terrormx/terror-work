import Link from "@/components/ui/link";
import { Business } from "@/types";

interface BusinessLinksProps {
  id: string;
  business?: Business;
}

function BusinessContact({ business, id }: BusinessLinksProps) {
  const prettyUrl =
    business?.url && new URL(business.url).hostname.replace(/www\./, "");

  if (!business?.verifiedAt) return null;

  return (
    <div className="p-4 text-sm flex flex-col">
      Enlaces
      {business?.ig && (
        <Link href={`https://instagram.com/${business.ig}`} target="_blank">
          ¹ @{business.ig}
        </Link>
      )}
      {prettyUrl && business.url && (
        <Link href={business.url} target="_blank">
          ² {prettyUrl}
        </Link>
      )}
    </div>
  );
}

export default BusinessContact;
