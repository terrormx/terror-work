import { useForm } from "react-hook-form";
import axios, { AxiosError } from "axios";
import { useRouter } from "next/router";
import { usePostHog } from "posthog-js/react";
import { z } from "zod";

import { zodResolver } from "@hookform/resolvers/zod";
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";

import BottomContainer from "./bottom-container";

import Button from "@/components/shared/button";
import {
  Form,
  FormControl,
  FormField,
  FormItem,
  FormMessage,
} from "@/components/ui/form";
import { Input } from "@/components/ui/input";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "@/components/ui/select";
import { Separator } from "@/components/ui/separator";
import { useToast } from "@/components/ui/use-toast";
import { queries } from "@/lib/queries";
import { BusinessFormSchema } from "@/lib/schemas";
import { createBusinessSlug } from "@/lib/utils";
import { Business } from "@/types";
import getFingerprint from "@/utils/fingerprint";

type FormData = z.infer<typeof BusinessFormSchema>;

export default function BusinessForm() {
  const router = useRouter();
  const { toast, dismiss } = useToast();
  const { categoryId, stateId } = router.query as Record<string, string>;
  const posthog = usePostHog();
  const queryClient = useQueryClient();
  const { data: states } = useQuery(queries.states.all);
  const form = useForm<FormData>({
    resolver: zodResolver(BusinessFormSchema),
    defaultValues: {
      categoryId,
      stateId,
    },
  });
  const { mutate, isPending, error } = useMutation({
    mutationFn: async (data: FormData): Promise<Business[]> => {
      const fingerprint = await getFingerprint();
      const response = await axios.post(
        "/api/businesses",
        {
          ...data,
          categoryId,
          stateId,
          labels: [],
        },
        {
          headers: {
            "X-Device-Fingerprint": fingerprint,
          },
        },
      );
      return response.data;
    },
    onSuccess: (data) => {
      const [newBusiness] = data;
      queryClient.invalidateQueries({
        queryKey: queries.businesses.list({ categoryId, stateId }).queryKey,
      });
      posthog.capture("Business Created", {
        categoryId,
        stateId,
      });
      router.replace(
        `/${stateId}/${categoryId}/${createBusinessSlug(newBusiness)}`,
      );
      toast({
        title: "¡Gracias!",
        description:
          "Sé el primero en reportar malas prácticas en este negocio",
        variant: "default",
        duration: 10000,
        icon: "Love",
      });
    },
    onError: (error: AxiosError) => {
      const statusCode = error?.response?.status;
      posthog.capture("Business Creation Failed", {
        categoryId,
        stateId,
        statusCode,
      });

      if (statusCode === 429) {
        router.replace(`/${stateId}/${categoryId}`);
        return toast({
          title: "Sólo puedes agregar un negocio",
          icon: "BlackCat",
        });
      }
      if (statusCode === 409) {
        router.replace(`/${stateId}/${categoryId}`);
        return toast({
          title: "Este negocio ya existe",
          icon: "Extinguisher",
        });
      }

      toast({
        title: "Algo salió mal. Intenta de nuevo",
        icon: "BlackCat",
      });
    },
  });

  const onSubmit = (data: FormData) => {
    mutate(data);
  };

  return (
    <Form {...form}>
      <form
        onClick={() => dismiss()}
        onSubmit={form.handleSubmit(onSubmit)}
        className="pb-28"
      >
        <FormField
          control={form.control}
          name="name"
          render={({ field }) => (
            <FormItem>
              <FormControl>
                <Input
                  placeholder="Nombre del negocio"
                  type="text"
                  id="name"
                  autoFocus
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <Separator />
        <FormField
          control={form.control}
          name="ig"
          render={({ field }) => (
            <FormItem>
              <FormControl>
                <Input
                  placeholder="Instagram"
                  type="text"
                  id="ig"
                  {...field}
                  onChange={(e) => {
                    field.onChange(e.target.value.toLowerCase());
                  }}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <Separator />
        <FormField
          control={form.control}
          name="url"
          render={({ field }) => (
            <FormItem>
              <FormControl>
                <Input
                  placeholder="Sitio web { opcional }"
                  type="url"
                  id="url"
                  {...field}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <Separator />
        <FormField
          control={form.control}
          name="rfc"
          render={({ field }) => (
            <FormItem>
              <FormControl>
                <Input
                  placeholder="RFC { opcional }"
                  type="text"
                  id="rfc"
                  {...field}
                  onChange={(e) => {
                    field.onChange(e.target.value.toUpperCase());
                  }}
                />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <Separator />
        <FormField
          control={form.control}
          name="categoryId"
          render={({ field }) => (
            <FormItem>
              <FormControl>
                <Input type="hidden" id="categoryId" disabled {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />
        <FormField
          control={form.control}
          name="stateId"
          render={({ field }) => (
            <FormItem>
              <FormControl>
                <Select {...field} disabled>
                  <SelectTrigger>
                    <SelectValue placeholder="ESTADO" />
                  </SelectTrigger>
                  <SelectContent>
                    {states?.map((s) => (
                      <SelectItem key={s.id} value={s.id}>
                        {s.name}
                      </SelectItem>
                    ))}
                  </SelectContent>
                </Select>
              </FormControl>
              <FormMessage />
            </FormItem>
          )}
        />

        <BottomContainer>
          <Button type="submit" disabled={isPending}>
            {isPending ? "Guardando..." : "Agregar"}
          </Button>
        </BottomContainer>
      </form>
    </Form>
  );
}
