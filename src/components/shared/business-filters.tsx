import { memo, useCallback } from "react";
import { useRouter } from "next/router";
import { usePostHog } from "posthog-js/react";

import { Badge as Badge } from "@/components/ui/badge";
import useLabels from "@/hooks/useLabels";

interface BusinessFiltersProps {
  categoryId: string;
  stateId: string;
}

function BusinessFilters({ categoryId, stateId }: BusinessFiltersProps) {
  const posthog = usePostHog();
  const router = useRouter();
  const { data: labels, isLoading } = useLabels();

  const handleLabelClick = useCallback(
    (label: string) => {
      const query = { ...router.query };
      router.query.label === label ? delete query.label : (query.label = label);

      router.replace({ query }, undefined, { shallow: true, scroll: false });

      posthog.capture("Tag Clicked", {
        label,
        categoryId,
        stateId,
      });
    },
    [categoryId, posthog, router, stateId],
  );

  if (isLoading) return null;

  return (
    <div className="flex gap-1 overflow-x-scroll p-2 sticky top-16 z-10 bg-category border-b border-b-dark">
      {labels?.parentLabels.map(({ id, name }) => {
        const isPresent = router.query.label === id;
        return (
          <Badge
            key={id}
            variant={isPresent ? "default" : "outline"}
            onClick={() => handleLabelClick(id)}
            className={
              isPresent
                ? `border-category text-category`
                : `border-dark text-dark bg-category`
            }
          >
            {name}
          </Badge>
        );
      })}
    </div>
  );
}

export default memo(BusinessFilters);
