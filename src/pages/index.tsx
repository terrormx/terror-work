import Link from "next/link";

import { dehydrate, QueryClient } from "@tanstack/react-query";

import BottomContainer from "@/components/shared/bottom-container";
import Button from "@/components/shared/button";
import Head from "@/components/shared/head";
import ShareButton from "@/components/shared/share-button";
import States from "@/components/shared/states";
import { queries } from "@/lib/queries";

export default function Home() {
  return (
    <div className="default bg-category min-h-screen pb-20">
      <Head />
      <States />
      <BottomContainer>
        <Link href="/core/manifesto" className="grow flex">
          <Button>Lee el manifesto</Button>
        </Link>
        <ShareButton />
      </BottomContainer>
    </div>
  );
}

export async function getStaticProps() {
  const queryClient = new QueryClient();
  await queryClient.fetchQuery(queries.states.all);

  return { props: { dehydratedState: dehydrate(queryClient) } };
}
