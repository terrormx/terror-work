import { useEffect, useState } from "react";
import type { AppProps } from "next/app";
import { useRouter } from "next/router";
import posthog from "posthog-js";
import { PostHogProvider } from "posthog-js/react";

import {
  HydrationBoundary,
  QueryClient,
  QueryClientProvider,
} from "@tanstack/react-query";
import { Analytics } from "@vercel/analytics/react";
import { SpeedInsights } from "@vercel/speed-insights/next";

import "@/lib/globals.css";
import Navbar from "@/components/shared/navbar";
import ThemeProvider from "@/components/shared/theme-provider";
import { Toaster } from "@/components/ui/toaster";
import { cn } from "@/lib/utils";
import fonts from "@/utils/fonts";

if (typeof window !== "undefined") {
  posthog.init(process.env.NEXT_PUBLIC_POSTHOG_KEY!, {
    api_host: process.env.NEXT_PUBLIC_POSTHOG_HOST || "https://app.posthog.com",
    // Enable debug mode in development
    loaded: (posthog) => {
      if (process.env.NODE_ENV === "development") posthog.debug();
    },
  });
}

export default function App({ Component, pageProps }: AppProps) {
  const router = useRouter();
  const [queryClient] = useState(
    () =>
      new QueryClient({
        defaultOptions: {
          queries: {
            staleTime: Infinity,
          },
        },
      }),
  );

  useEffect(() => {
    const handleRouteChange = () => posthog?.capture("$pageview", router.query);
    router.events.on("routeChangeComplete", handleRouteChange);
    return () => {
      router.events.off("routeChangeComplete", handleRouteChange);
    };
  }, [router.events]);

  return (
    <QueryClientProvider client={queryClient}>
      <HydrationBoundary state={pageProps.dehydratedState}>
        <PostHogProvider client={posthog}>
          <ThemeProvider />
          <Analytics />
          <SpeedInsights />
          <div
            className={cn(
              "min-h-screen text-dark font-sans bg-category flex flex-col polka",
              fonts.sans.variable,
              fonts.display.variable,
            )}
          >
            <Navbar />
            <Component {...pageProps} />
            <Toaster />
          </div>
        </PostHogProvider>
      </HydrationBoundary>
    </QueryClientProvider>
  );
}
