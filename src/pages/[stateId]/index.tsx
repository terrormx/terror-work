import Link from "next/link";
import { useRouter } from "next/router";

import { dehydrate, QueryClient, useQuery } from "@tanstack/react-query";

import Button from "@/components/shared/button";
import Categories from "@/components/shared/categories";
import Head from "@/components/shared/head";
import ShareButton from "@/components/shared/share-button";
import useOgImage from "@/hooks/useOgImage";
import { queries } from "@/lib/queries";
import { State } from "@/types";
import api from "@/utils/axios/client";

export default function StatePage() {
  const router = useRouter();
  const { stateId } = router.query;
  const { data: states } = useQuery(queries.states.all);
  const state = states?.find((s) => s.id === stateId);
  const ogImage = useOgImage({
    name: state?.name,
  });

  return (
    <div className="pb-20">
      <Head title={`Terror ${state?.name}`} ogImage={ogImage} />
      <Categories stateId={state?.id} />
      <div className="fixed bottom-1 left-1 right-1 flex">
        <Link href="/core/manifesto" className="grow flex">
          <Button className="w-full">Lee el manifesto</Button>
        </Link>
        <ShareButton />
      </div>
    </div>
  );
}

export async function getStaticPaths() {
  const response = await api.get<State[]>("states");
  const states = response.data;

  const paths = states
    .filter((state) => state.active)
    .map((state) => ({
      params: { stateId: state.id },
    }));

  return { paths, fallback: false };
}

export async function getStaticProps() {
  const queryClient = new QueryClient();
  await queryClient.fetchQuery(queries.states.all);
  await queryClient.fetchQuery(queries.categories.all);

  return { props: { dehydratedState: dehydrate(queryClient) } };
}
