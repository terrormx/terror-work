import Link from "next/link";
import { useRouter } from "next/router";
import { usePostHog } from "posthog-js/react";

import { dehydrate, QueryClient, useQuery } from "@tanstack/react-query";

import BottomContainer from "@/components/shared/bottom-container";
import Businesses from "@/components/shared/businesses";
import Button from "@/components/shared/button";
import Head from "@/components/shared/head";
import ShareButton from "@/components/shared/share-button";
import useOgImage from "@/hooks/useOgImage";
import { queries } from "@/lib/queries";
import { Category, State } from "@/types";
import api from "@/utils/axios/client";

export default function CategoryPage() {
  const { query } = useRouter();
  const posthog = usePostHog();
  const { stateId, categoryId } = query as Record<string, string>;
  const { data: categories } = useQuery(queries.categories.all);
  const category = categories?.find((c) => c.id === categoryId);
  const ogImage = useOgImage({
    categoryId: categoryId as string,
  });

  return (
    <div className="flex flex-col grow pb-20">
      <Head title={`Terror ${category?.name}`} ogImage={ogImage} />
      <Businesses
        categoryId={categoryId as string}
        stateId={stateId as string}
      />

      {posthog.isFeatureEnabled("business-creation") && (
        <BottomContainer>
          <Link
            href={`/${stateId}/${categoryId}/businesses/new`}
            className="grow flex"
          >
            <Button>Agrega</Button>
          </Link>
          <ShareButton />
        </BottomContainer>
      )}
    </div>
  );
}

export async function getStaticPaths() {
  const states = await api.get<State[]>("states");
  const categories = await api.get<Category[]>("categories");
  const params: Array<[string, string]> = [];
  states.data.forEach((state) => {
    return categories.data
      .filter((category) => category.active)
      .forEach((category) => {
        params.push([state.id, category.id]);
      });
  });

  const paths = params.map(([stateId, categoryId]) => ({
    params: { stateId, categoryId },
  }));

  return { paths, fallback: false };
}

export async function getStaticProps() {
  const queryClient = new QueryClient();
  await queryClient.fetchQuery(queries.categories.all);

  return { props: { dehydratedState: dehydrate(queryClient) } };
}
