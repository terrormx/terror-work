import { dehydrate, QueryClient } from "@tanstack/react-query";

import Form from "@/components/shared/business-form";
import Head from "@/components/shared/head";
import { Alert, AlertDescription } from "@/components/ui/alert";
import { queries } from "@/lib/queries";
import { Category, State } from "@/types";
import api from "@/utils/axios/client";

export default function NewBusiness() {
  return (
    <main>
      <Head title="Nuevo negocio" />
      <Alert>
        <AlertDescription className="uppercase">
          Te pedimos que verifiques si el negocio ya existe antes de agregarlo.
        </AlertDescription>
      </Alert>
      <Form />
    </main>
  );
}

export async function getStaticPaths() {
  const states = await api.get<State[]>("states");
  const categories = await api.get<Category[]>("categories");
  const params: Array<[string, string]> = [];
  states.data.forEach((state) => {
    return categories.data.forEach((category) => {
      params.push([state.id, category.id]);
    });
  });

  const paths = params.map(([stateId, categoryId]) => ({
    params: { stateId, categoryId },
  }));

  return { paths, fallback: false };
}

export async function getStaticProps() {
  const queryClient = new QueryClient();
  await queryClient.fetchQuery(queries.categories.all);

  return { props: { dehydratedState: dehydrate(queryClient) } };
}
