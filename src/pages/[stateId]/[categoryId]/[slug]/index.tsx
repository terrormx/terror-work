import { NextPageContext } from "next";
import NextLink from "next/link";
import { useRouter } from "next/router";

import {
  dehydrate,
  QueryClient,
  useInfiniteQuery,
  useQuery,
} from "@tanstack/react-query";

import BottomContainer from "@/components/shared/bottom-container";
import BusinessAlert from "@/components/shared/business-alert";
import BusinessContact from "@/components/shared/business-contact";
import BusinessFilters from "@/components/shared/business-filters";
import Button from "@/components/shared/button";
import Head from "@/components/shared/head";
import Reports from "@/components/shared/reports";
import ShareButton from "@/components/shared/share-button";
import useOgImage from "@/hooks/useOgImage";
import { queries } from "@/lib/queries";
import { isClient, parseBusinessSlug } from "@/lib/utils";
import { PAGE_SIZE } from "@/lib/utils";
import { Report } from "@/types";

export default function Business() {
  const { query } = useRouter();
  const { slug, categoryId, stateId, label } = query as Record<string, string>;
  const { name, id } = parseBusinessSlug(slug);
  const ogImage = useOgImage({ categoryId, name });

  const { data: business } = useQuery(queries.businesses.detail(id));
  const { data: count } = useQuery(
    queries.businesses.detail(id)._ctx.reports(label)._ctx.count(),
  );
  const { data, fetchNextPage, hasNextPage, isLoading } = useInfiniteQuery<
    Report[]
  >({
    ...queries.businesses.detail(id)._ctx.reports(label),
    initialPageParam: 0,
    getNextPageParam: (lastPage, allPages) => {
      const loadedItems = (allPages?.length ?? 0) * PAGE_SIZE;
      if (loadedItems < (count ?? 0)) return loadedItems;
    },
  });

  const reports = data?.pages.flat();

  return (
    <div className="flex flex-col grow pb-20">
      <Head title={`Terror en ${name}`} ogImage={ogImage} />
      <BusinessAlert count={business?.reportCount} id={id} />
      <BusinessFilters categoryId={categoryId} stateId={stateId} />
      <Reports
        data={reports ?? []}
        categoryId={categoryId}
        slug={slug}
        stateId={stateId}
        isLoading={isLoading}
        fetchNextPage={fetchNextPage}
        hasNextPage={hasNextPage}
      />
      {!isLoading && <BusinessContact business={business} id={id} />}

      <BottomContainer>
        <NextLink
          href={`/${stateId}/${categoryId}/${slug}/reports/new`}
          className="grow flex"
        >
          <Button>Denunciar</Button>
        </NextLink>
        <ShareButton />
      </BottomContainer>
    </div>
  );
}

Business.getInitialProps = async (ctx: NextPageContext) => {
  if (isClient(ctx)) return {};

  const { id } = parseBusinessSlug(ctx.query.slug as string);
  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(queries.categories.all);
  await queryClient.prefetchQuery(queries.businesses.detail(id));
  await queryClient.prefetchInfiniteQuery({
    ...queries.businesses.detail(id)._ctx.reports(ctx.query.label as string),
    initialPageParam: 0,
  });

  return { dehydratedState: dehydrate(queryClient) };
};
