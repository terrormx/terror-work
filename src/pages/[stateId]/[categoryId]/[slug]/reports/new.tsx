import { NextPageContext } from "next";

import { dehydrate, QueryClient } from "@tanstack/react-query";

import Head from "@/components/shared/head";
import Form from "@/components/shared/report-form";
import { Alert, AlertDescription } from "@/components/ui/alert";
import { queries } from "@/lib/queries";
import { isClient } from "@/lib/utils";

export default function NewReport() {
  return (
    <main>
      <Head title="Nuevo reporte" />
      <Alert>
        <AlertDescription>
          <p>
            El objetivo de las denuncias son las empresas, no las individuos.
            Evita mencionar nombres o datos personales.
          </p>
        </AlertDescription>
      </Alert>
      <Form />
    </main>
  );
}

NewReport.getInitialProps = async (ctx: NextPageContext) => {
  if (isClient(ctx)) return {};

  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(queries.categories.all);

  return { dehydratedState: dehydrate(queryClient) };
};
