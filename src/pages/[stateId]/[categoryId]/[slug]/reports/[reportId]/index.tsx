import { NextPageContext } from "next";
import { useRouter } from "next/router";
import { usePostHog } from "posthog-js/react";

import { dehydrate, QueryClient, useQuery } from "@tanstack/react-query";

import BottomContainer from "@/components/shared/bottom-container";
import Button from "@/components/shared/button";
import Head from "@/components/shared/head";
import IgReport from "@/components/shared/ig-report";
import Report from "@/components/shared/report";
import ShareButton from "@/components/shared/share-button";
import useOgImage from "@/hooks/useOgImage";
import useShare from "@/hooks/useShare";
import { queries } from "@/lib/queries";
import { isClient, parseBusinessSlug } from "@/lib/utils";

export default function ReportPage() {
  const router = useRouter();
  const posthog = usePostHog();
  const { slug, reportId, category } = router.query as Record<string, string>;
  const { name } = parseBusinessSlug(slug);
  const ogImage = useOgImage({ categoryId: category, name });
  const { isAvailable, shareImage, isSharing } = useShare();
  const { data: report } = useQuery(queries.reports.detail(reportId));
  const { data: labels } = useQuery({
    ...queries.labels.all,
    select: (data) => data.filter((label) => report?.labels.includes(label.id)),
    enabled: !!report,
  });
  const shareLabel =
    posthog.getFeatureFlag("share-image-copy") === "ig"
      ? "Compartir en Instagram"
      : "Compartir imagen";

  return (
    <div className="pb-20">
      <Head title={`Terror en ${name}`} ogImage={ogImage} />
      <Report
        title={report?.title}
        body={report?.body}
        hasProof={report?.hasProof}
        createdAt={report?.createdAt}
        labels={labels}
      />
      {isAvailable && (
        <BottomContainer>
          <Button onClick={() => shareImage(reportId)} disabled={isSharing}>
            {isSharing ? "Exportando..." : shareLabel}
          </Button>
          <ShareButton />
        </BottomContainer>
      )}

      {/* Layer on top to avoid scrolling and revealing the hidden image */}
      <div className="bg-category-darker fixed top-0 left-0 w-full h-full z-[-10]" />

      {report?.body && (
        <IgReport
          id={reportId}
          businessName={name}
          body={report.body}
          className="fixed top-0 z-[-10000]"
        />
      )}
    </div>
  );
}

ReportPage.getInitialProps = async (ctx: NextPageContext) => {
  if (isClient(ctx)) return {};

  const queryClient = new QueryClient();
  await queryClient.prefetchQuery(queries.categories.all);
  await queryClient.prefetchQuery(
    queries.reports.detail(ctx.query.reportId as string),
  );

  return { dehydratedState: dehydrate(queryClient) };
};
