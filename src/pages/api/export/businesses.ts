import { NextApiRequest, NextApiResponse } from "next";

import { generateCSV } from "@/lib/utils";
import { Business } from "@/types";
import api from "@/utils/axios/server";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  try {
    const params: Record<string, string> = {};
    if (req.query.category) params.category_id = `eq.${req.query.category}`;
    if (req.query.state) params.state_id = `eq.${req.query.state}`;

    const response = await api.get<Business[]>("businesses", { params });
    const csv = generateCSV(response.data);
    res.setHeader("Content-Type", "text/csv");
    res.setHeader("Content-Disposition", "attachment; filename=businesses.csv");
    res.status(200).send(csv);
  } catch (error) {
    res.status(500).json({ message: "Internal server error" });
  }
}
