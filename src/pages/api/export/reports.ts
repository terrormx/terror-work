import { NextApiRequest, NextApiResponse } from "next";

import { generateCSV } from "@/lib/utils";
import { Report } from "@/types";
import api from "@/utils/axios/server";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  try {
    const params: Record<string, string> = {};
    if (req.query.business) params.business_id = `eq.${req.query.business}`;

    const response = await api.get<Report[]>("reports", { params });
    const csv = generateCSV(response.data);
    res.setHeader("Content-Type", "text/csv");
    res.setHeader("Content-Disposition", "attachment; filename=reports.csv");
    res.status(200).send(csv);
  } catch (error) {
    res.status(500).json({ message: "Internal server error" });
  }
}
