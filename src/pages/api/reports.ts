import { NextApiRequest, NextApiResponse } from "next";

import * as Sentry from "@sentry/nextjs";

import { ReportFormSchema } from "@/lib/schemas";
import api from "@/utils/axios/server";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  if (req.method !== "POST")
    return res.status(405).json({ message: "Method not allowed" });

  const result = ReportFormSchema.safeParse(req.body);
  if (!result.success) return res.status(422).json(result.error.issues);

  try {
    const response = await api.post("reports", result.data);
    res.status(200).json(response.data);
  } catch (err: any) {
    Sentry.captureException(err);
    res.status(500).json({ message: "Internal server error" });
  }
}
