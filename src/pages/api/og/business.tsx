import { NextApiRequest } from "next";
import { ImageResponse } from "next/og";

import RuledPage from "@/components/shared/og/ruled-page";
import Title from "@/components/shared/og/title";
import * as Icons from "@/components/ui/icon";

export const config = {
  runtime: "edge",
};

export default async function handler(req: NextApiRequest) {
  const url = new URL(req.url ?? "");
  const searchParams = new URLSearchParams(url.search);
  const { color, name } = Object.fromEntries(searchParams.entries());
  const fontData = await fetch(
    new URL("/public/fonts/ribes/Ribes-Black.otf", import.meta.url),
  ).then((res) => res.arrayBuffer());

  return new ImageResponse(
    (
      <RuledPage color={color && `#${color}`}>
        <Title>{name}</Title>
        <Icons.DestroyColonialism
          width={380}
          style={{
            position: "absolute",
            bottom: 32,
            right: 32,
          }}
        />
      </RuledPage>
    ),
    {
      // debug: true,
      width: 1200,
      height: 630,
      fonts: [
        {
          name: "Ribes",
          data: fontData,
          style: "normal",
        },
      ],
    },
  );
}
