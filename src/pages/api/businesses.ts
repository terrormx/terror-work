import { NextApiRequest, NextApiResponse } from "next";

import * as Sentry from "@sentry/nextjs";

import { BusinessFormSchema } from "@/lib/schemas";
import api from "@/utils/axios/server";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  if (req.method !== "POST")
    return res.status(405).json({ message: "Method not allowed" });

  const result = BusinessFormSchema.safeParse(req.body);
  if (!result.success) return res.status(422).json(result.error.issues);

  try {
    const response = await api.post("businesses", result.data);
    res.status(200).json(response.data);
  } catch (err: any) {
    Sentry.captureException(err);
    if (err.response.data.code === "23505") {
      return res.status(409).json({ message: "Already exists" });
    }

    res.status(500).json({ message: "Internal server error" });
  }
}
