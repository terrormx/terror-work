import { useRouter } from "next/router";

import BottomContainer from "@/components/shared/bottom-container";
import Button from "@/components/shared/button";
import EmptyState from "@/components/shared/empty-state";
import { BlackCat } from "@/components/ui/icon";

export default function NotFound() {
  const router = useRouter();
  return (
    <div className="lines default bg-category grow pb-20">
      <EmptyState message="No existe la página que buscas" />
      <BlackCat
        cutOut
        className="rotate-12 w-2/3 max-w-40 absolute bottom-28 right-8"
      />
      <BottomContainer>
        <Button onClick={router.back}>Regresar</Button>
      </BottomContainer>
    </div>
  );
}
