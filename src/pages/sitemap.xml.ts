import { GetServerSideProps } from "next";

import { createBusinessSlug } from "@/lib/utils";
import { Business, Category, State } from "@/types";
import api from "@/utils/axios/client";

const URL = "https://terror.work";

interface Options {
  states: State[];
  categories: Category[];
  businesses: Business[];
}

function generateSiteMap({ states, categories, businesses }: Options) {
  return `<?xml version="1.0" encoding="UTF-8"?>
   <urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9">
    
    <url>
        <loc>${URL}</loc>
    </url>
    <url>
       <loc>${URL}/core/somos-todos</loc>
    </url>
    <url>
       <loc>${URL}/core/manifesto</loc>
    </url>
    <url>
       <loc>${URL}/core/principios</loc>
    </url>

     
    ${states
      .map(
        ({ id }) => `
    <url>
        <loc>${`${URL}/${id}`}</loc>
    </url>
    `,
      )
      .join("")}
    
    
    ${states
      .map(({ id: stateId }) =>
        categories
          .map(
            ({ id: categoryId }) => `
    <url>
        <loc>${`${URL}/${stateId}/${categoryId}`}</loc>
    </url>
    `,
          )
          .join(""),
      )
      .join("")}

    
    ${businesses
      .map((business) => {
        console.log(business.name);
        return `
        <url>
            <loc>${`${URL}/${business.stateId}/${business.categoryId}/${createBusinessSlug(business)}`}</loc>
        </url>
      `;
      })
      .join("")}
   </urlset>
 `;
}

export const getServerSideProps: GetServerSideProps<{}> = async ({ res }) => {
  const statesRes = await api.get("states", {
    params: {
      select: "id",
    },
  });
  const states = statesRes.data;
  const categoriesRes = await api.get("categories", {
    params: {
      select: "id",
    },
  });
  const categories = categoriesRes.data;
  const businessesRes = await api.get("businesses", {
    params: {
      select: "id,state_id,category_id,name",
    },
  });
  const businesses = businessesRes.data;

  const sitemap = generateSiteMap({
    states,
    categories,
    businesses,
  });

  res.setHeader("Content-Type", "text/xml");
  res.write(sitemap);
  res.end();

  return {
    props: {},
  };
};

export default function SiteMap() {}
