import { useQuery } from "@tanstack/react-query";

import { queries } from "@/lib/queries";

interface Options {
  categoryId?: string;
  name?: string;
}

const useOgImage = ({ categoryId, name }: Options): string | undefined => {
  const { data: categories } = useQuery(queries.categories.all);
  const category = categories?.find((c) => c.id === categoryId);
  const color = category?.color.replace("#", "");

  if (category && name) {
    return `https://terror.work/api/og/business?name=${name}&color=${color}`;
  }

  if (category) {
    return `https://terror.work/api/og/category?name=${category.name}&color=${color}`;
  }

  return `https://terror.work/api/og/state?name=${name}`;
};

export default useOgImage;
