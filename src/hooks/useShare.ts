import { useState } from "react";
import * as htmlToImage from "html-to-image";
import { useRouter } from "next/router";
import { usePostHog } from "posthog-js/react";

import { parseBusinessSlug } from "@/lib/utils";

let hasLoadedFonts = false;

function useShare() {
  const [isSharing, setIsSharing] = useState(false);
  const { query, asPath } = useRouter();
  const posthog = usePostHog();
  const { slug, categoryId, stateId, reportId } = query as Record<
    string,
    string
  >;
  const { id: businessId } = parseBusinessSlug(slug);
  const isAvailable =
    typeof window !== "undefined" && !!window?.navigator?.share;

  const shareURL = async () => {
    if (!isAvailable) return;
    setIsSharing(true);

    posthog.capture("Share Button Clicked", {
      businessId,
      categoryId,
      stateId,
      reportId,
      type: "url",
    });

    const params = {
      utm_source: "webapp",
      utm_medium: "share",
      utm_content:
        (reportId && "report") ||
        (businessId && "business") ||
        (categoryId && "category") ||
        (stateId && "state"),
    };

    try {
      await navigator.share({
        title: document.querySelector("title")?.innerText,
        url:
          "https://terror.work" +
          asPath +
          "?" +
          new URLSearchParams(params).toString(),
      });
      posthog.capture("URL Shared", {
        businessId,
        categoryId,
        stateId,
        reportId,
      });
    } catch (error) {
      console.error("Error sharing image", error);
    }
    setIsSharing(false);
  };

  const shareImage = async (elementId: string) => {
    const element = document.getElementById(elementId);
    if (!element || !isAvailable) return;

    setIsSharing(true);
    posthog.capture("Share Button Clicked", {
      businessId,
      categoryId,
      stateId,
      reportId,
      type: "image",
    });
    if (!hasLoadedFonts) {
      // The library is not loading fonts the first time.
      await htmlToImage.toBlob(element);
      hasLoadedFonts = true;
    }
    const blob = await htmlToImage.toBlob(element);

    if (blob) {
      let file = new File(
        [blob],
        `${document.querySelector("title")?.innerText}.jpg`,
        { type: "image/jpeg" },
      );
      // Instagram only accepts 1 file. Do not send other attrs.
      // TODO: Allow downloading multiple files.
      try {
        await navigator.share({ files: [file] });
        posthog.capture("Image Shared", {
          businessId,
          categoryId,
          stateId,
          reportId,
        });
      } catch (error) {
        console.error("Error sharing image", error);
      }
    }

    setIsSharing(false);
  };
  return {
    isAvailable,
    shareURL,
    shareImage,
    isSharing,
  };
}

export default useShare;
