import { useQuery } from "@tanstack/react-query";

import { queries } from "@/lib/queries";
import { Label } from "@/types";

function useLabels() {
  return useQuery({
    ...queries.labels.all,
    select: (data) => {
      const parentLabels: Label[] = [];
      const childrenLabels: Record<string, Label[]> = {};

      data.forEach((label) => {
        if (!label.parentId) return parentLabels.push(label);

        if (childrenLabels[label.parentId]) {
          childrenLabels[label.parentId].push(label);
        } else {
          childrenLabels[label.parentId] = [label];
        }
      });
      return { parentLabels, childrenLabels };
    },
  });
}

export default useLabels;
