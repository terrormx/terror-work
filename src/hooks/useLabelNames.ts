import { useQuery } from "@tanstack/react-query";

import { queries } from "@/lib/queries";
import { Label } from "@/types";

type Dictionary = Record<string, string>;

function useLabelNames() {
  const { data } = useQuery({
    ...queries.labels.all,
    select: (data) => {
      return data.reduce((dictionary: Dictionary, label: Label) => {
        dictionary[label.id] = label.name;
        return dictionary;
      }, {});
    },
  });

  return data ?? {};
}

export default useLabelNames;
