import axios from "axios";
import { camelizeKeys, decamelizeKeys } from "humps";

const api = axios.create({
  baseURL: `${process.env.NEXT_PUBLIC_SUPABASE_URL}/rest/v1/`,
  headers: {
    apikey: process.env.NEXT_PUBLIC_SUPABASE_ANON_KEY,
    Authorization: `Bearer ${process.env.NEXT_PUBLIC_SUPABASE_SERVICE_ROLE}`,
    "Content-Type": "application/json",
    Prefer: "return=representation",
  },
  transformRequest: [(data) => JSON.stringify(decamelizeKeys(data))],
  transformResponse: [
    (data) => {
      if (data) {
        return camelizeKeys(JSON.parse(data));
      }
    },
  ],
});

export default api;
