import FingerprintJS from "@fingerprintjs/fingerprintjs";

const getFingerprint = async () => {
  const fpPromise = FingerprintJS.load();
  const fp = await fpPromise;
  const result = await fp.get();
  return result.visitorId;
};

export default getFingerprint;
