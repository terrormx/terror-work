create table
  public.labels (
    id character varying not null,
    created_at timestamp with time zone not null default now(),
    name character varying not null,
    parent_id character varying null,
    constraint labels_pkey primary key (id),
    constraint labels_parent_id_fkey foreign key (parent_id) references labels (id)
  ) tablespace pg_default;