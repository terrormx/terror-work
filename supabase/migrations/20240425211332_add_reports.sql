create table
  public.reports (
    id uuid not null default gen_random_uuid (),
    created_at timestamp with time zone not null default now(),
    updated_at timestamp with time zone null default now(),
    business_id uuid not null,
    title character varying not null,
    body text not null,
    on_behalf boolean not null default false,
    has_proof boolean not null default false,
    labels character varying[] not null default '{}'::character varying[],
    user_id uuid null,
    seniority smallint not null default '0'::smallint,
    constraint reports_pkey primary key (id),
    constraint reports_business_id_fkey foreign key (business_id) references businesses (id),
    constraint reports_user_id_fkey foreign key (user_id) references auth.users (id) on delete set null
  ) tablespace pg_default;

create trigger report_created
after insert on reports for each row
execute function increment_report_count ();