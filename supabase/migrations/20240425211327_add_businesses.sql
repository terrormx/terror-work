create table
  public.businesses (
    id uuid not null default gen_random_uuid (),
    created_at timestamp with time zone not null default now(),
    updated_at timestamp with time zone null default now(),
    rfc character varying null,
    url character varying null,
    ig character varying not null,
    name character varying not null,
    state_id character varying not null,
    category_id character varying not null,
    report_count bigint not null default '0'::bigint,
    verified_at timestamp with time zone null,
    constraint businesses_pkey primary key (id),
    constraint businesses_url_key unique (url),
    constraint unique_business_ig_per_state unique (ig, state_id),
    constraint unique_business_rfc_per_state unique (rfc, state_id),
    constraint businesses_category_id_fkey foreign key (category_id) references categories (id),
    constraint businesses_state_id_fkey foreign key (state_id) references states (id)
  ) tablespace pg_default;