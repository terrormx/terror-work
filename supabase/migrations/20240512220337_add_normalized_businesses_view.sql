CREATE view
  normalized_businesses AS
SELECT
  unaccent(name) AS normalized_name,
  *
FROM
  businesses;