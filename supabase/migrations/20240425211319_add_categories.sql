create table
  public.categories (
    id character varying not null,
    created_at timestamp with time zone not null default now(),
    name character varying not null,
    active boolean null default false,
    color character varying not null default '#EBE3D4'::character varying,
    constraint categories_pkey primary key (id)
  ) tablespace pg_default;