create table
  public.states (
    id character varying not null,
    created_at timestamp with time zone not null default now(),
    updated_at timestamp with time zone null,
    name character varying null,
    active boolean not null default false,
    constraint states_pkey primary key (id)
  ) tablespace pg_default;